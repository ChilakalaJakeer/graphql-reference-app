import gql from "graphql-tag";

export const booksQuery = gql`
  {
    books {
      name
      id
    }
  }
`;

export const authorsQuery = gql`
  {
    authors {
      name
      id
    }
  }
`;

export const bookQuery = gql`
  query($Id: ID) {
    book(id: $Id) {
      name
      genre
      author {
        name
        books {
          id
          name
          genre
        }
      }
    }
  }
`;
export const authorMutation = gql`
  mutation($name: String!, $age: Int!) {
    addAuthor(name: $name, age: $age) {
      name
      age
    }
  }
`;
export const bookMutation = gql`
  mutation($name: String!, $genre: String!, $authorId: String!) {
    addBook(name: $name, genre: $genre, authorId: $authorId) {
      name
      genre
    }
  }
`;
