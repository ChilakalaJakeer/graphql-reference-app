import React, { useState } from "react";
import { useQuery, useMutation } from "@apollo/react-hooks";
import { authorsQuery, bookMutation } from "../queries/graphql.queries";

function AddBook() {
  const { loading: loadingAuthors, data: authorsQueryRes } = useQuery(
    authorsQuery
  );

  const [addBook, { data: bookMutationRes }] = useMutation(bookMutation);

  const Authors = () => {
    if (loadingAuthors) {
      return <option disabled>Loading authors ...</option>;
    } else {
      return authorsQueryRes.authors.map(author => (
        <option key={author.id} value={author.id}>
          {author.name}
        </option>
      ));
    }
  };

  const [name, setName] = useState("");
  const [genre, setGenre] = useState("");
  const [author, setAuthor] = useState(" ");
  const onSubmit = e => {
    e.preventDefault();
    name &&
      genre &&
      author &&
      addBook({ variables: { name, genre, authorId: author } });
    setName("");
    setGenre("");
    setAuthor("");
  };

  return (
    <div className="AddBook">
      <form id="addbook" onSubmit={onSubmit}>
        <h3>Book Form</h3>
        <div className="field">
          <label>Name</label>
          <input
            type="text"
            value={name}
            onChange={e => setName(e.target.value)}
            placeholder=" Book name"
          />
        </div>
        <div className="field">
          <label>Genre</label>
          <input
            type="text"
            value={genre}
            onChange={e => setGenre(e.target.value)}
            placeholder=" Genre"
          />
        </div>
        <div className="field">
          <label>Author</label>
          <select
            value={author}
            onChange={e => setAuthor(e.target.value)}
            id="select"
          >
            <option>Select author</option>
            <Authors />
          </select>
        </div>
        <button> Add book</button>
      </form>
    </div>
  );
}

export default AddBook;
