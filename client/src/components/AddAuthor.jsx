import React, { useState } from "react";
import { useMutation } from "@apollo/react-hooks";
import { authorMutation } from "../queries/graphql.queries";

function AddAuthor() {
  const [addAuthor, { data: authorMutationRes }] = useMutation(authorMutation);

  const [name, setName] = useState("");
  const [age, setAge] = useState("");

  const onSubmit = e => {
    e.preventDefault();
    name && age && addAuthor({ variables: { name, age } });
    setName("");
    setAge("");
  };

  return (
    <div className="AddAuthor">
      <form id="addauthor" onSubmit={onSubmit}>
        <h3>Author Form</h3>
        <div className="field">
          <label>Name</label>
          <input
            type="text"
            value={name}
            onChange={e => setName(e.target.value)}
            placeholder=" Author name"
          />
        </div>
        <div className="field">
          <label>Age</label>
          <input
            type="number"
            value={age}
            onChange={e => setAge(parseInt(e.target.value))}
            placeholder=" Author age"
          />
        </div>

        <button> Add author</button>
      </form>
    </div>
  );
}

export default AddAuthor;
