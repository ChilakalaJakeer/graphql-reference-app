import React, { useState } from "react";
import { useQuery } from "@apollo/react-hooks";
import { booksQuery } from "../queries/graphql.queries";
import BookDetails from "./BookDetails";

function BookList() {
  const { loading: loadingBooks, data: booksQueryRes } = useQuery(booksQuery);
  const [selected, setSelected] = useState("");
  if (loadingBooks) {
    return (
      <div style={{ margin: "117px 50%" }}>
        <div className="lds-ellipsis">
          <div></div>
          <div></div>
          <div></div>
          <div></div>
        </div>
      </div>
    );
  } else {
    return (
      <div className="DataDisplay">
        <div>
          <h3>Books</h3>
          <ul>
            {booksQueryRes.books.map(book => (
              <li key={book.id} onClick={() => setSelected(book.id)}>
                {book.name}
              </li>
            ))}
          </ul>
        </div>
        <BookDetails bookId={selected} />
      </div>
    );
  }
}

export default BookList; //binding query to component
