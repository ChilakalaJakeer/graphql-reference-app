import React from "react";
import { useQuery } from "@apollo/react-hooks";
import { bookQuery } from "../queries/graphql.queries";

function BookDetails({ bookId }) {
  const {
    loading: bookQueryLoading,
    error,
    data: bookQueryRes
  } = useQuery(bookQuery, { variables: { Id: bookId } });
  console.log(bookQueryRes);

  const Details = () => {
    if (bookQueryRes) {
      return (
        <div>
          <h3>Book :{bookQueryRes.book.name}</h3>
          <h4>Author : {bookQueryRes.book.author.name}</h4>
          other books by author
          <table align="center">
            <thead>
              <tr>
                <th>Book</th>
                <th>Genre</th>
              </tr>
            </thead>
            <tbody>
              {bookQueryRes.book.author.books.map(book => (
                <tr key={book.id}>
                  <td>{book.name}</td>
                  <td>{book.genre}</td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      );
    } else {
      return <div>no book selected</div>;
    }
  };

  return (
    <div>
      <Details />
    </div>
  );
}

export default BookDetails;
