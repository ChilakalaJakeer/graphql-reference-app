import React from "react";
import ApolloClient from "apollo-boost";
import { ApolloProvider } from "@apollo/react-hooks";

import BookList from "./components/BookList";
import { SERVER_URI } from "./utils/config";
import AddBook from "./components/AddBook";
import AddAuthor from "./components/AddAuthor";

const apolloClient = new ApolloClient({
  uri: SERVER_URI
});

function App() {
  return (
    <div className="App">
      <ApolloProvider client={apolloClient}>
        <h1>GraphQL Client - BookList</h1>
        <BookList />
        <div className="forms">
          <AddBook />
          <AddAuthor />
        </div>
      </ApolloProvider>
    </div>
  );
}

export default App;
