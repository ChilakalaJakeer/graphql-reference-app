const express = require("express");
require("dotenv").config();
const graphqlHTTP = require("express-graphql"); //graphql express  intigration module
const mongoose = require("mongoose");
const cors = require("cors");

const schema = require("./schema/schema");

const app = express();

//static pages
app.use(express.static("public/html"));

//cors middleware
app.use(cors());

//mongoDB connection
const MongoDB = process.env.ATLAS_MONGODB_URL;
mongoose.connect(
  MongoDB,
  { useNewUrlParser: true, useUnifiedTopology: true },
  () => console.log(MongoDB)
);
//graphql endpoint
app.use(
  "/graphql",
  graphqlHTTP({
    schema,
    graphiql: true
  })
);

const PORT = process.env.PORT | 7000;

app.listen(PORT);
